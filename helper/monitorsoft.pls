﻿<?xml version="1.0" encoding="UTF-8"?>
<lexicon
      xmlns="http://www.w3.org/2005/01/pronunciation-lexicon"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.w3.org/2005/01/pronunciation-lexicon
      http://www.w3.org/TR/2007/CR-pronunciation-lexicon-20071212/pls.xsd"
      alphabet="x-sampa" xml:lang="ru-RU">
<!--

30/06/2015
--><lexeme>
	<grapheme>Геленджика</grapheme>
	<phoneme>gelendz`1k'a</phoneme>
	<grammar>NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>Ноябрьска</grapheme>
	<phoneme>n@j'abr' sk@</phoneme>
	<grammar>NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>Нягани</grapheme>
	<phoneme>njAgani</phoneme>
	<grammar>NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>Перми</grapheme>
	<phoneme>perm'i</phoneme>
	<grammar>NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>Родоса</grapheme>
	<phoneme>r'od@sa</phoneme>
	<grammar>NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>Симферополя</grapheme>
	<phoneme>simfer'opolja</phoneme>
	<grammar>NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>Стамбула</grapheme>
	<phoneme>stamb'ul@</phoneme>
	<grammar>NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>Сэвэн</grapheme>
	<phoneme>s'E vEn</phoneme>
	<grammar>NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>Талакана</grapheme>
	<phoneme>talak'an@</phoneme>
	<grammar>NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>Усинска</grapheme>
	<phoneme>us'insk@</phoneme>
	<grammar>NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>Ханты Мансийска</grapheme>
	<phoneme>hant1 mans'ijsk@</phoneme>
	<grammar>CONTRACTION NOUN   NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>Ямбурга</grapheme>
	<phoneme>'jamburg@</phoneme>
	<grammar>NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>Авиа</grapheme>
	<phoneme>Avia</phoneme>
	<grammar>NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>Декстер</grapheme>
	<phoneme>dEkstEr</phoneme>
	<grammar>NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>Роял</grapheme>
	<phoneme>r'Ojall</phoneme>
	<grammar>NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>Ларнаки</grapheme>
	<phoneme>larn'aki</phoneme>
	<grammar>NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>Нарьян мара</grapheme>
	<phoneme>nar'jan m'Ar@</phoneme>
	<grammar>NOUN   NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>Производится</grapheme>
	<phoneme>pr@izv'odits@</phoneme>
	<grammar>VERB</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>Самары</grapheme>
	<phoneme>sam'ar1</phoneme>
	<grammar>NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>регистрация</grapheme>
	<phoneme>registr'ats1ja</phoneme>
	<grammar>NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
<lexeme>
	<grapheme>рейс</grapheme>
	<phoneme>r'ej s</phoneme>
	<grammar>NOUN</grammar>
	<lexname>monitorsoft</lexname>
	<notes>новый</notes>
	<color>0xc7ffcc</color>
</lexeme>
</lexicon>