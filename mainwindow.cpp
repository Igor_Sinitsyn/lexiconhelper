#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QAxObject>
#include <QDebug>
#include <QFile>
#include <QHBoxLayout>
#include <QDomDocument>
#include <QFileDialog>

#include <windows.h>
#include <sphelper.h>

#include "labelwidget.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

	CoInitialize(NULL);
	m_sapi = new QAxObject("SAPI.SpVoice");

	loadVoices();
	initHelperTable();
	loadHelperTable(EN_GB);

	initPlsTable();

	connect(ui->a_playTranscription, SIGNAL(clicked()), this, SLOT(on_a_playTranscriptionClicked()));
	connect(ui->a_playOriginal, SIGNAL(clicked()), this, SLOT(on_a_playOriginalClicked()));
	connect(ui->f_voiceLanguage, SIGNAL(currentIndexChanged(int)), this, SLOT(on_f_voiceLanguageItemIndexChanged(int)));
	connect(ui->a_loadFromFile, SIGNAL(clicked()), this, SLOT(on_a_loadFromFileClicked()));
	connect(ui->a_saveToFile, SIGNAL(clicked()), this, SLOT(on_a_saveToFileClicked()));
	connect(ui->g_plsTable, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(on_plsTable_cellDoubleClicked(int, int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::loadVoices()
{
	QAxObject *voice = m_sapi->querySubObject("GetVoices()");
	if (voice->dynamicCall("Count").toInt() == -1) {
		qWarning() << "Not fount install voices";
	}
	else {
		for (int i = 0; i < voice->dynamicCall("Count").toInt(); i++) {
			ui->f_voiceName->addItem(voice->querySubObject(QString("Item(" + QString::number(i) + ")").toLatin1())->dynamicCall("GetDescription()").toString());
		}
	}

	ui->f_voiceLanguage->addItem("en-US");	
	ui->f_voiceLanguage->addItem("ru-RU");
	ui->f_voiceLanguage->addItem("en-GB");
}

void MainWindow::initHelperTable()
{
	QStringList columns;
	columns << tr("X-SAMPA") << tr("Description") << tr("Example");

	ui->g_helperTable->setColumnCount(3);
	ui->g_helperTable->setHorizontalHeaderLabels(columns);
}

void MainWindow::loadHelperTable(Language lang)
{
	ui->g_helperTable->clearContents();
	QString fileName = QString("helper/%1.helper");
	if (lang == EN_US) {
		fileName = fileName.arg("en-US");
	} else if (lang == RU_RU) {
		fileName = fileName.arg("ru-RU");
	}
	else if (lang == EN_GB) {
		fileName = fileName.arg("en-GB");
	}
	QFile file(fileName);
	if (file.open(QIODevice::ReadOnly)) {
		QString data = file.readAll();
		
		for (int i = 0; i < data.split("\n").size(); i++) {			
			if (data.split("\n").at(i).split(";").size() == 3) {
				ui->g_helperTable->setRowCount(i + 1);
				
				ui->g_helperTable->setCellWidget(i, 0, new LabelWidget(data.split("\n").at(i).split(";").at(0)));

				ui->g_helperTable->setCellWidget(i, 1, new LabelWidget(data.split("\n").at(i).split(";").at(1)));

				ui->g_helperTable->setCellWidget(i, 2, new LabelWidget(data.split("\n").at(i).split(";").at(2)));
			}
		}
		file.close();
	} else {
		qDebug() << Q_FUNC_INFO << "File " << fileName << " not opened";
	}
}

void MainWindow::initPlsTable()
{
	QStringList columns;
	columns << tr("Grapheme") << tr("Phoneme") << tr("Grammar");

	ui->g_plsTable->setColumnCount(3);
	ui->g_plsTable->setHorizontalHeaderLabels(columns);
}

void MainWindow::addPlsTableRow(const QString &grapheme, const QString &phoneme, const QString &grammar)
{
	int index = ui->g_plsTable->rowCount();
	ui->g_plsTable->setRowCount(index + 1);

	ui->g_plsTable->setCellWidget(index, 0, new LabelWidget(grapheme));

	ui->g_plsTable->setCellWidget(index, 1, new LabelWidget(phoneme));

	ui->g_plsTable->setCellWidget(index, 2, new LabelWidget(grammar));
}

void MainWindow::textToSpeech(const QString &text, const QString &transcription, const QString &language)
{
	HRESULT                      hr = S_OK;
	CComPtr<ISpPhoneConverter>   cpPhoneConv;
	CComPtr<ISpLexicon>          cpLexicon;
	LANGID                       langidUS;
	SPPHONEID                    wszId[SP_MAX_PRON_LENGTH];

	hr = cpLexicon.CoCreateInstance(CLSID_SpLexicon);

	// 0x409 for English.
	langidUS = MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US);

	if (SUCCEEDED(hr)) {
		hr = SpCreatePhoneConverter(langidUS, NULL, NULL, &cpPhoneConv);
	}

	if (SUCCEEDED(hr)) {
		hr = cpPhoneConv->PhoneToId((const wchar_t*)transcription.utf16(), wszId);
	}
	if (SUCCEEDED(hr)) {
		hr = cpLexicon->AddPronunciation((const wchar_t*)text.utf16(), langidUS, SPPS_Noun, wszId);
	}

	m_sapi->setProperty("Voice", m_sapi->querySubObject("GetVoices()")->
		querySubObject(QString("Item(").append(QString::number(ui->f_voiceName->currentIndex())).
		append(")").toLatin1())->asVariant());

	m_sapi->setProperty("Rate", QString::number(0));
	m_sapi->setProperty("Volume", QString::number(100));

	m_sapi->dynamicCall("Speak(\"const QString&\")", "red");

	hr = cpLexicon->RemovePronunciation((const wchar_t*)text.utf16(), langidUS, SPPS_Noun, wszId);
}

void MainWindow::on_a_playTranscriptionClicked()
{
	textToSpeech(ui->f_original->text(), ui->f_transcription->text(), "");
}

void MainWindow::on_a_playOriginalClicked()
{
	m_sapi->setProperty("Voice", m_sapi->querySubObject("GetVoices()")->
		querySubObject(QString("Item(").append(QString::number(ui->f_voiceName->currentIndex())).
		append(")").toLatin1())->asVariant());

	m_sapi->setProperty("Rate", QString::number(0));
	m_sapi->setProperty("Volume", QString::number(100));

	m_sapi->dynamicCall("Speak(\"const QString&\")", ui->f_original->text());
}

void MainWindow::on_f_voiceLanguageItemIndexChanged(int index)
{
	loadHelperTable((Language)index);	
}

void MainWindow::on_a_loadFromFileClicked()
{
	QString fileName = QFileDialog::getOpenFileName(this, "Open Pls file",
		"", "Pls Files (*.pls)");
	QFile file(fileName);
	QString lang;
	if (file.open(QIODevice::ReadOnly)) {
		QDomDocument doc;
		QString error;
		int column, line;
		if (doc.setContent(file.readAll(), &error, &line, &column)) {
			QDomElement element = doc.documentElement();
			lang = element.attribute("xml:lang", "en-US");
			if (element.attribute("xml:lang", "en-US") == "en-US") {
				ui->f_voiceLanguage->setCurrentIndex((int)EN_GB);
			} else if (element.attribute("xml:lang", "en-US") == "ru-RU") {
				ui->f_voiceLanguage->setCurrentIndex((int)RU_RU);
			} else if (element.attribute("xml:lang", "en-US") == "en-GB") {
				ui->f_voiceLanguage->setCurrentIndex((int)EN_GB);
			}

			QDomNodeList list = element.elementsByTagName("lexeme");
			for (int i = 0; i < list.size(); i++) {
				QDomNode node = list.at(i).firstChild();
				QString grapheme, phoneme, grammar;
				while (!node.isNull()) {
					qDebug() << node.toElement().tagName();
					if (node.toElement().tagName() == "grapheme") {
						grapheme = node.toElement().text();
					} else if (node.toElement().tagName() == "phoneme") {
						phoneme = node.toElement().text();
					} else if (node.toElement().tagName() == "grammar") {
						grammar = node.toElement().text();
					}
					node = node.nextSibling();
				}

				addPlsTableRow(grapheme, phoneme, grammar);
			}
						
		}
		ui->g_fileName->setText(fileName + " <b>(" + lang + ")</b>");
		file.close();
	} else {
		qDebug() << Q_FUNC_INFO << "File " << fileName << " not opened";
	}
}

void MainWindow::on_a_saveToFileClicked()
{
	QString fileName = QFileDialog::getSaveFileName(this, "Open Pls file",
		"", "Pls Files (*.pls)");

	QDomDocument doc;	
	doc.appendChild(doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\""));
	QDomElement lexicon = doc.createElement("lexicon");

	lexicon.setAttribute("xlmns", "http://www.w3.org/2005/01/pronunciation-lexicon");
	lexicon.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
	lexicon.setAttribute("xsi:schemaLocation", "http://www.w3.org/2005/01/pronunciation-lexicon http://www.w3.org/TR/2007/CR-pronunciation-lexicon-20071212/pls.xsd");
	lexicon.setAttribute("alphabet", "x-sampa");
	lexicon.setAttribute("xml:lang", ui->f_voiceLanguage->currentText());

	for (int i = 0; i < ui->g_plsTable->rowCount(); i++) {
		QDomElement lexeme = doc.createElement("lexeme");

		QDomElement element;
		QDomText text;

		element = doc.createElement("grapheme");
		text = doc.createTextNode(reinterpret_cast<LabelWidget*>(ui->g_plsTable->cellWidget(i, 0))->text());
		element.appendChild(text);
		lexeme.appendChild(element);

		element = doc.createElement("phoneme");
		text = doc.createTextNode(reinterpret_cast<LabelWidget*>(ui->g_plsTable->cellWidget(i, 1))->text());
		element.appendChild(text);
		lexeme.appendChild(element);

		element = doc.createElement("grammar");
		text = doc.createTextNode(reinterpret_cast<LabelWidget*>(ui->g_plsTable->cellWidget(i, 2))->text());
		element.appendChild(text);
		lexeme.appendChild(element);

		element = doc.createElement("lexname");
		text = doc.createTextNode("MonitorSoft");
		element.appendChild(text);
		lexeme.appendChild(element);

		lexicon.appendChild(lexeme);
	}

	doc.appendChild(lexicon);

	QFile file(fileName);
	if (file.open(QIODevice::WriteOnly)) {
		file.write(doc.toString().toUtf8());
		file.close();
	} else {
		qDebug() << Q_FUNC_INFO << "File " << fileName << " not opened";
	}
}

void MainWindow::on_plsTable_cellDoubleClicked(int row, int column)
{
	Q_UNUSED(column);
	ui->f_original->setText(reinterpret_cast<LabelWidget*>(ui->g_plsTable->cellWidget(row, 0))->text());
	ui->f_transcription->setText(reinterpret_cast<LabelWidget*>(ui->g_plsTable->cellWidget(row, 1))->text());
}