#-------------------------------------------------
#
# Project created by QtCreator 2015-07-06T16:02:24
#
#-------------------------------------------------

QT       += core gui

QT += axcontainer

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LexiconHelper
TEMPLATE = app

INCLUDEPATH += "c:/Program Files/Microsoft SDKs/Windows/v7.1/Include"
LIBS += -L"c:/Program Files/Microsoft SDKs/Windows/v7.1/Lib"


SOURCES += main.cpp\
        mainwindow.cpp \
    labelwidget.cpp

HEADERS  += mainwindow.h \
    labelwidget.h

FORMS    += mainwindow.ui
