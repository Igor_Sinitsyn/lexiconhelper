#ifndef LABELWIDGET_H
#define LABELWIDGET_H

#include <QWidget>
#include <QHBoxLayout>
#include <QLabel>

class LabelWidget : public QWidget
{
    Q_OBJECT
public:
    explicit LabelWidget(QString caption, QWidget *parent = 0);
	~LabelWidget();
	QString text();
signals:

public slots:
private:
	QHBoxLayout *m_layout;
	QLabel *m_label;

};

#endif // LABELWIDGET_H
