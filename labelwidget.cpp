#include "labelwidget.h"

LabelWidget::LabelWidget(QString caption, QWidget *parent) : QWidget(parent)
{
	m_layout = new QHBoxLayout(this);	
	m_layout->setAlignment(Qt::AlignCenter);
	m_layout->setContentsMargins(0, 0, 0, 0);
	this->setLayout(m_layout);	

	m_label = new QLabel(caption);
	m_layout->addWidget(m_label);
}

LabelWidget::~LabelWidget()
{
	delete m_label;
	delete m_layout;
}

QString LabelWidget::text()
{
	return m_label->text();
}

