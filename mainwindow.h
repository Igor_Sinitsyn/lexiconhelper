#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QAxObject>

namespace Ui {
class MainWindow;
}

enum Language {
	EN_US = 0, RU_RU = 1, EN_GB = 2
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
	QAxObject *m_sapi;

	void loadVoices();
	void initHelperTable();
	void loadHelperTable(Language lang);
	void initPlsTable();    
	void addPlsTableRow(const QString &grapheme, const QString &phoneme, const QString &grammar);
	void textToSpeech(const QString &text, const QString &transcription, const QString &language);
private slots:
	void on_a_playTranscriptionClicked();
	void on_a_playOriginalClicked();
	void on_f_voiceLanguageItemIndexChanged(int index);
	void on_a_loadFromFileClicked();
	void on_a_saveToFileClicked();
	void on_plsTable_cellDoubleClicked(int row, int column);
};

#endif // MAINWINDOW_H
